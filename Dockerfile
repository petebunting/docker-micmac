FROM ubuntu:latest

LABEL authors="Pete Bunting"
LABEL maintainer="petebunting@mac.com"

#Install dependencies
RUN apt-get update && apt-get install -y --install-recommends \
x11proto-core-dev make cmake libx11-dev imagemagick gcc g++ \
exiv2 libimage-exiftool-perl libgeo-proj4-perl \ 
mesa-common-dev libgl1-mesa-dev libglapi-mesa libglu1-mesa \
qt5-default doxygen opencl-headers git graphviz gnupg gnupg2 gnupg1 vim wget

RUN echo "deb http://ppa.launchpad.net/ubuntugis/ppa/ubuntu bionic main" | tee -a /etc/apt/sources.list
RUN echo "deb-src http://ppa.launchpad.net/ubuntugis/ppa/ubuntu bionic main" | tee -a /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 314DF160
RUN apt-get update && apt-get install -y --install-recommends ossim-core pdal gdal-bin python3-gdal

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Moving to /opt directory where files are to be stored.
WORKDIR /opt

#get micmac
RUN git clone https://github.com/micmacIGN/micmac.git micmac

#folders etc

RUN mkdir -p micmac/build

WORKDIR /opt/micmac

#make micmac without gpu 

RUN cmake -DWITH_QT5=1 -DBUILD_RNX2RTKP=1 -DBUILD_POISSON=1 -DWITH_OPENCL=ON -DWITH_OPEN_MP=OFF -DWITH_ETALONPOLY=ON -DWITH_DOXYGEN=ON .

RUN make
RUN make install

ENV PATH=$PATH:/opt/micmac/bin

# change to opt directory
WORKDIR /opt

# Clone Ciaran's Scripts.
RUN git clone https://github.com/petebunting/Sfm.git sfm_drone

# Make scripts executable
RUN chmod +x /opt/sfm_drone/*.sh /opt/sfm_drone/*.py /opt/sfm_drone/substages/*.py /opt/sfm_drone/substages/*.sh

ENV PATH=$PATH:/opt/sfm_drone:/opt/sfm_drone/substages

WORKDIR /opt/sfm_drone

RUN wget https://raw.githubusercontent.com/ossimlabs/ossim/dev/share/ossim/templates/ossim_preferences_template

ENV OSSIM_PREFS_FILE=/opt/sfm_drone/ossim_preferences_template

WORKDIR /opt

# Add Tini
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

CMD [ "/bin/bash" ]
